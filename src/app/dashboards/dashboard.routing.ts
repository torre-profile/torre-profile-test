import { Routes } from '@angular/router';

// import { DashboardAdmComponent } from './dashboard-adm/dashboard-adm.component';

export const DashboardRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'dashboard-adm',
                // component: DashboardAdmComponent,
                data: {
                    title: 'Principal',
                    //urls: [{ title: 'Principal', url: '/dashboard-adm' }, { title: 'Principal' }]
                }
            }
        ]
    }
];
