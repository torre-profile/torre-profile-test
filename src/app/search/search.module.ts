import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
// import { 
    // PersonalComponent,
    // SkillsComponent
    //  } from './components';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        ReactiveFormsModule,
        SearchRoutingModule
    ],
    declarations: [
        SearchComponent,
        // PersonalComponent,
        // SkillsComponent
    ],
    exports: [SearchComponent]
})
export class SearchModule { }
