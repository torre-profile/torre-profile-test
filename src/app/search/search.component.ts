import {
    Component, OnInit
} from '@angular/core';
import { Observable } from 'rxjs';
import { 
    // LocalService,
    TorreSearchService
    } from '../shared';
import { environment } from '../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
    selector: 'app-search',
    styleUrls: ['./search.component.scss'],
    templateUrl: './search.component.html',
    providers: [
        TorreSearchService
    ]
})
export class SearchComponent implements OnInit {
    readonly VERSION = 'version';
    readonly EXPIRATION_DATE = 'expiration';
    readonly COTIZACION_DATA = 'persondata';
    readonly SESSION_TIMEOUT = 15;

    formEntity = this.formBuilder.group({
        parameter: new FormControl('')
    });
    
    get parameter() {return this.formEntity.get('parameter')}

    public total;
    public results;

    public prev;
    public next;
    public offset;

    public constructor(
        public router: Router,
        private formBuilder: FormBuilder,
        private tsearchService: TorreSearchService,
        // private localService: LocalService,
        ) {
    }

    ngOnInit(): void {
        // this.onSearch();
    }

    onSearch() {
        this.tsearchService
            .search(this.parameter.value, '')
            .subscribe(result =>
                this.setResult(result))
    }

    setResult(result) {
        this.total = result.total;
        this.results = result.results;
        this.prev = result.pagination.previous;
        this.next = result.pagination.next;
        this.offset = result.offset;
    }

    getCompensation(compensantions) {
        let compensation = '-.-';
        if (compensantions.employee != undefined) {
            compensation = compensantions.employee.currency + ' ' + compensantions.employee.amount;
        } else if(compensantions.freelancer != undefined) {
            compensation = compensantions.freelancer.currency + ' ' + compensantions.freelancer.amount;
        }else if(compensantions.intern != undefined) {
            compensation = compensantions.intern.currency + ' ' + compensantions.intern.amount;
        }

        return compensation;
    }

    getPeriodicy(compensantions) {
        let periodicity = '-.-';
        if (compensantions.employee != undefined) {
            periodicity = compensantions.employee.periodicity;
        } else if(compensantions.freelancer != undefined) {
            periodicity = compensantions.freelancer.periodicity;
        }else if(compensantions.intern != undefined) {
            periodicity = compensantions.intern.periodicity;
        }

        return periodicity;
    }
    
    onStep(step) {
        this.tsearchService
            .search(this.parameter.value, step)
            .subscribe(result =>
                this.setResult(result))
    }

    openProfile(profile) {
        const url = `/profile/${profile}`;
        this.router.navigate([url]);
    }
}
