import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NotFoundComponent } from './404/not-found.component';
import { LockComponent } from './lock/lock.component';
import { LoginAdmComponent } from './login-adm/login-adm.component';

import { SignupComponent } from './signup/signup.component';
import { Signup2Component } from './signup2/signup2.component';

import { AuthenticationRoutes } from './authentication.routing';
// import { NgxSoapModule } from 'ngx-soap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthenticationRoutes),
    // NgxSoapModule,
    NgbModule
  ],
  declarations: [
    NotFoundComponent,
    LoginAdmComponent,
    SignupComponent,
    LockComponent,
    Signup2Component
  ]
})

export class AuthenticationModule {}
