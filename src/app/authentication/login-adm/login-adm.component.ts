import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../shared/guard/auth.guard';
// import { UsuarioService, Usuario } from '../../shared';
import { TestSoapService } from '../../shared';
// import { NgxSoapService, Client, ISoapMethodResponse } from 'ngx-soap';

@Component({
    selector: 'app-login-adm',
    templateUrl: './login-adm.component.html',
    styleUrls: ['./login-adm.component.css'],
    providers: [
        // UsuarioService,
        // AuthService
        TestSoapService,
        // NgxSoapService
    ]
})
export class LoginAdmComponent implements OnInit, AfterViewInit {
    public error = false;
    // client: Client;

    constructor(public router: Router,
        private testService: TestSoapService,
        // private soap: NgxSoapService
        // private usuarioService: UsuarioService,
        // private auth0: AuthService
        ) {}

    ngOnInit() {
        localStorage.clear();
        localStorage.setItem('profile', Profile.admin.toString());
        localStorage.setItem('social', '0');
    }

    ngAfterViewInit() {
        /*$(function() {
            $(".preloader").fadeOut();
        });

        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });*/
    }

    onLoggedin(user, pass) {
        // const usuarioLog = new Usuario();
        // usuarioLog.usuario = user;
        // usuarioLog.contrasena = pass;

        // this.usuarioService.getList().subscribe(usuarios => {
        //     console.log(usuarios);
        // });
        // this.usuarioService
        //     .login(usuarioLog)
        //     .subscribe(usuario => {
        //         if (usuario.id > 0) {
        //             localStorage.setItem('user', usuario.id + '');
        //             localStorage.setItem('isLoggedin', 'true');
        //             localStorage.setItem('profile', Profile.admin.toString());
        //             this.router.navigate(['/dashboard/dashboard-adm']);
        //         }
        //     }
        // );


        // this.usuarioService.login(usuario).then(
        //     response => {
        //         if (response) {
        //             const id = response.id;
        //             localStorage.setItem('user', id);
        //             localStorage.setItem('isLoggedin', 'true');
        //             localStorage.setItem('profile', Profile.admin.toString());
        //             this.router.navigate(['/dashboard/dashboard-adm']);
        //         } else {
        //             this.error = true;
        //         }
        //     }
        // ).catch(e => this.handleError(e));

        this.testService.getTest();
        // this.soap.createClient('http://190.57.230.114:10704/Demonte.asmx?WSDL')
        //     .then(client => {
        //         console.log('Client', client);
        //         this.client = client;
        //     })
        //     .catch(err => console.log('Error', err));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
