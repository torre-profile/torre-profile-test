import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { 
    PersonalComponent,
    SkillsComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        ReactiveFormsModule,
        ProfileRoutingModule
    ],
    declarations: [
        ProfileComponent,
        PersonalComponent,
        SkillsComponent
    ],
    exports: [ProfileComponent]
})
export class ProfileModule { }
