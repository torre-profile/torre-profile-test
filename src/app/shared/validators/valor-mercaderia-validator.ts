import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

const MENOR_A_VALOR_QUIEBRE = '0';
export function ValorMercaderiaValidator(): ValidatorFn {
    return (form: FormGroup) : ValidationErrors | null => {
        let retValue = null;
        const menor = form.get("valormenor").value;
        const valor = form.get("valormercaderia").value;
        const quiebre = form.get("quiebre").value;

        if(menor == MENOR_A_VALOR_QUIEBRE) {
            if(valor == undefined || +valor < 1) {
                retValue = { required: true };
            } else if(valor >= quiebre) {
                form.get("valormercaderia").setErrors({ superaQuiebre: true });
                retValue = { superaQuiebre: true };
            }
        }
        return retValue;
    }
}
