import {AbstractControl, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export function ElectrodomesticosValidator(): ValidatorFn {
    return (form: FormGroup) : ValidationErrors | null => {

        const tipo = form.get("electrodomestico").value;
        if (tipo == null || tipo == 'null') {
            return { required: true } ;
        }

        let value = null;
        let required = false;
        switch(+tipo) {
            case 1:
                value = form.get('tamanosplit').value;
                required = true;
                break;
            case 2:
                value = form.get('tamanofreezer').value;
                required = true;
                break;
            case 3:
                value = form.get('tamanoheladera').value;
                required = true;
                break;
            case 4:
                value = form.get('tamanotermotanque').value;
                required = true;
                break;
            case 5:
                value = form.get('tamanotv').value;
                required = true;
                break;
        }

        return (required && (value == 'null' || value == null)) ? { required: true } : null;
    }
}