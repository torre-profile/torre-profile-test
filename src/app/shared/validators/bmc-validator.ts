import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export function BMCValidator(): ValidatorFn {
    return (form: FormGroup) : ValidationErrors | null => {

        const tipo = form.get("tipobmc").value;
        if (tipo == null || tipo == 'null') {
            return { required: true } ;
        }

        let value = null;
        let required = false;
        switch(tipo) {
            case '1':
                value = form.get('rodado').value;
                required = true;
                break;
            case '2':
                value = form.get('cilindradasmoto').value;
                required = true;
                break;
            case '3':
                value = form.get('cilindradascuatri').value;
                required = true;
                break;
        }

        return (required && (value == 'null' || value == null)) ? { required: true } : null;
    }
}