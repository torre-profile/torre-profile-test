import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

const FACTURA_REMITENTE = '0';

export function EntregaValidator(): ValidatorFn {
    return (form: FormGroup) : ValidationErrors | null => {
         
        let tipo = null;
        let prefix = '';

        tipo = (form.get("facturaa").value) ? form.get("facturaa").value : null;
        if(tipo) {
            prefix = (tipo == FACTURA_REMITENTE) ? 'rem_' : 'dest_';
        } else {
            return { required: true } ;
        }

        let returnValue = null; 
        // busco todos los controles con el prefijo y verifico que no sea vacío.
        Object.keys(form.controls).forEach(key => {
            if(key.startsWith(prefix)) {
                if(key.indexOf('localidad') < 0) {
                    if(form.controls[key].value == undefined) {
                        returnValue =  { required: true };
                    } else if(form.controls[key].value.length == 0) {
                        returnValue =  { required: true };
                    }
                }
            }
        });
        return returnValue;
    }
}
