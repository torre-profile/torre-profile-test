import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function DocumentoValidator(tipo:string): ValidatorFn {
 
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
    
        if (!value) {
            return {required: true};
        }
        if(tipo == '0') {
            if(value.length != 11) {
                return {DocumentoValidator: true, lengthError:true};
            }
            let acumulado = 0
            let digitos = value.toString().split('')// value.split();
            const digito = digitos.pop();

            for(var i = 0; i < digitos.length; i++)  {
                acumulado += digitos[9 - i] * (2 + (i % 6));
            }
            var verif = 11 - (acumulado % 11);
            if(verif == 11) {
                verif = 0;
            } else if(verif == 10) {
                verif = 9;
            }
            return digito == verif ? null : {DocumentoValidator: true, incorrectCuit:true}; 
        } else {
            if(value.length > 6 && value.length < 9)
                return null;
            else 
                return {DocumentoValidator: true, lengthError:true};
        }
        return null
    }
}
