import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function LocalidadValidator(locsArray): ValidatorFn {
 
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
    
        if(!locsArray) {
            return { 'required': true };
        }
        if (value == undefined) {
            return null;
        } else if(value == '') {
            return { 'required': true };
        }

        const loc = locsArray.find(localidad => localidad.localidad == value);
        if(loc) {
            return null;
        }

        return { 'required': true };
    }
}
