import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function EmailValidator(): ValidatorFn {
 
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
        let retValue = null;
    
        if (control.value) {
            const matches = control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
            retValue =  matches ? null : { 'invalidEmail': true };
        }
        return retValue;
    }
}
