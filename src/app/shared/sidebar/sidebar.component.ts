import { Component, AfterViewInit, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';

import { Profile } from '../guard/auth.guard';
// import { Profesional, ProfesionalService, Usuario, UsuarioService } from '../';

declare var $: any;
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    providers: [
        // ProfesionalService,
        // UsuarioService
        ]
})
export class SidebarComponent implements OnInit {
    showMenu = '';
    showSubMenu = '';
    public sidebarnavItems: any[];
    selectedID = '';
    selectedProfile: Profile;
    // selectedEntity: Usuario;
    // selectedUsuario: Usuario;
    nombrePerfil: string;
    locacionPerfil: string;

    // this is for the open close
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';

        } else {
            this.showMenu = element;
        }
    }

    addActiveClass(element: any) {
        if (element === this.showSubMenu) {
            this.showSubMenu = '0';

        } else {
            this.showSubMenu = element;
        }
    }

    constructor(private modalService: NgbModal, private router: Router,
        private route: ActivatedRoute,
        // private profesionalService: ProfesionalService,
        // private usuarioService: UsuarioService
        ) {
            // this.selectedEntity = new Usuario();
    }
    // End open close
    ngOnInit() {
        this.selectedID = localStorage.getItem('user');
        this.onGetEntityData();
        this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
        $(function () {
            $('.sidebartoggler').on('click', function () {
                if ($('#main-wrapper').hasClass('mini-sidebar')) {
                    $('body').trigger('resize');
                    $('#main-wrapper').removeClass('mini-sidebar');

                } else {
                    $('body').trigger('resize');
                    $('#main-wrapper').addClass('mini-sidebar');
                }
            });

        });
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            this.getAdmin();
        }
    }

    getAdmin() {
        // this.usuarioService
        //     .getByID(this.selectedID)
        //     .subscribe(usuario => {
        //         this.selectedEntity = usuario;
        //         this.nombrePerfil = this.selectedEntity.usuario;
        //         this.selectedEntity.fotourl = this.selectedEntity.fotourl;
        //     });
    }

    public onLogout() {
        this.router.navigate(['/authentication/login-adm']);
      localStorage.clear();
    }

    public hasImage() {
        // return (this.selectedEntity.fotourl) ? (this.selectedEntity.fotourl.length > 0) : false;
    }
}
