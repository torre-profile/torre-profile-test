import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../shared/guard/auth.guard';
// import * as auth0 from 'auth0-js';
// import { ProfesionalService, Profesional } from '../services';
// import { Paciente, PacienteService } from '../api-services/paciente.service';

(window as any).global = window;

@Injectable({
  providedIn: 'root'
})

export class AuthService {
    private _idToken: string;
    private _accessToken: string;
    private _expiresAt: number;
    // private auth0: auth0.WebAuth;
    // private authOptions: auth0.AuthOptions;

    private profile: Profile;
    userProfile: any;

    // auth0 = new auth0.WebAuth({
    //     clientID: environment.authConfig.clientID,
    //     domain: environment.authConfig.domain,
    //     responseType: 'token id_token',
    //     redirectUri: environment.authConfig.callbackURL,
    //     scope: 'openid profile'
    // });

    constructor(public router: Router,
        // private profesionalService: ProfesionalService
        ) {

        // this.authOptions = {
        //     domain: environment.authConfig.domain,
        //     clientID: environment.authConfig.clientID,
        //     scope: 'openid profile'
        // };

        // this.auth0 = new auth0.WebAuth(this.authOptions);

        this._idToken = '';
        this._accessToken = '';
        this._expiresAt = 0;
    }

    get accessToken(): string {
        return this._accessToken;
    }

    get idToken(): string {
        return this._idToken;
    }

    public login(profile: Profile): void {
        this.profile = profile;
        localStorage.setItem('profile', this.profile.toString());
        // this.auth0.authorize({
        //     responseType: 'token id_token',
        //     redirectUri: environment.authConfig.callbackURL
        // });
    }

    signUp(profile: Profile) {
        this.profile = profile;
        localStorage.setItem('profile', this.profile.toString());
        // this.auth0.authorize({
        //     responseType: 'token id_token',
        //     redirectUri: environment.authConfig.callbackURL
        // });
    }

    handleAuthentication(): void {
        // this.auth0.parseHash((err, authResult) => {
        //     if (authResult && authResult.accessToken && authResult.idToken) {
        //         this.setSession(authResult);

        //         this.getProfile((err1: any, profile1: any) => {
        //             if (err1 !== null) { console.log(err1); }
        //             if (localStorage.getItem('isSingingUp') === '1') {
        //                 this.signUpSocial(profile1);
        //             } else {
        //                 this.loginSocial(profile1);
        //             }
        //         });
        //     } else if (err) {
        //         this.router.navigate(['/authentication/404']);
        //         console.log(err);
        //         alert(`Error: ${err.error}. Check the console for further details.`);
        //     }
        // });
    }

    // private loginSocial(profile: any): any {
    //     switch (localStorage.getItem('profile')) {
    //         case Profile.professional:
    //             this.loginSocialPro(profile);
    //             break;
    //         case Profile.admin:
    //             // this.loginAdm(profile1);
    //             break;
    //     }
    // }

    // private loginSocialPro(profile: any): any {
    //     const idsocial = profile.sub.replace('|', '-');
    //     this.profesionalService.getByIdSocial(idsocial).then(
    //         response => {
    //             if (response) {
    //                 if (response.idsocial === idsocial) {
    //                     const id = response.id;
    //                     localStorage.setItem('user', id);
    //                     localStorage.setItem('isLoggedin', 'true');
    //                     localStorage.setItem('profile', Profile.professional.toString());
    //                     this.redirectToDashborads();
    //                 }
    //             }
    //         }
    //     );
    // }

    private signUpSocial(profile: any): any {
        switch (localStorage.getItem('profile')) {
            case Profile.professional:
                this.signUpPro(profile);
                break;
            case Profile.admin:
                // this.loginAdm(profile1);
                break;
        }
    }

    private signUpPro(profile: any): any {
        // const entity = new Profesional();
        // entity.id =  localStorage.getItem('user');
        // entity.idsocial = profile.sub.replace('|', '-');
        // entity.fotourl = profile.picture;
        // this.profesionalService.loginSocial(entity).then(
        //     response => {
        //         if (response) {
        //             const id = response.id;
        //             localStorage.setItem('user', entity.id);
        //             localStorage.setItem('isLoggedin', 'true');
        //             localStorage.setItem('profile', Profile.professional.toString());
        //             if (localStorage.getItem('isSingingUp') === '1') {
        //                 this.router.navigate(['/perfilprofesional']);
        //             } else {
        //                 this.redirectToDashborads();
        //             }
        //         } else {
        //             localStorage.setItem('user', entity.id);
        //             localStorage.setItem('usuario', entity.usuario);
        //             localStorage.setItem('fotourl', entity.fotourl);
        //             localStorage.setItem('apellido', entity.apellido);
        //             localStorage.setItem('nombre', entity.nombre);
        //             localStorage.setItem('social', '1');
        //             this.router.navigate(['/authentication/signup-pro']);
        //         }
        //         localStorage.removeItem('isSingingUp');
        //     }
        // );
    }

    private redirectToDashborads() {
        switch (localStorage.getItem('profile')) {
            case Profile.admin:
                this.router.navigate(['/dashboard/dashboard-adm']);
                break;
        }
    }

    private setSession(authResult): void {
        // Set the time that the access token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
    }

    public getProfile(cb): void {
        this._accessToken = localStorage.getItem('access_token');
        if (!this._accessToken) {
            throw new Error('Access token must exist to fetch profile');
        }

        const self = this;
        // this.auth0.client.userInfo(this._accessToken, (err, profile) => {
        //     if (profile) {
        //         self.userProfile = profile;
        //     }
        //     cb(err, profile);
        // });
    }

    public logout(): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
    }

    public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at') || '{}');
        return new Date().getTime() < expiresAt;
    }

}
