export enum ResponseCode {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NO_CONTENT = 204,
    NOT_MODIFIED = 304,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404
};

export enum TipoEnvio {
    otros = 0,
    mueble = 1,
    indumentaria = 2,
    informatica = 3,
    electrodomesticos = 4,
    motoreslanchas = 5,
    articulospersonales = 6,
    cubiertas = 7,
    bmc = 8
};
