import { environment } from '../../../environments/environment';

export class BasicService {
    public readonly SYNCRO_OK = 200;
    public readonly ERROR_DUPLICATED = -98;
    public readonly ERROR_UNKNOWN = -99;
    public readonly ERROR_FOREING_KEY = 202;
    public readonly ENTITY_DELETED = 204;
    public readonly ERROR_NOT_MODIFIED = 304;

    protected handleError(error: any): Promise<any> {
        if (!environment.production) {
            console.error('An error occurred', error);
        }

        if (error) {
            switch (error.status) {
                case 500:
                    return Promise.reject('Error de comunicación con el servidor. Error: 504!');
                default:
                return Promise.reject(error.message || error);
            }
        } else {
            return Promise.reject(error.message || error);
        }
    }
}
