import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';

import {from} from 'rxjs';

import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export class Strengths {
    public id: string;
    public code: number;
    public name: string;
    public proficiency: string;
    public weight: number;
    public recommendations: number;
    public created: string;
}

export class Person {
    public id: string;
    public picture: string;
    public name: string;
    public publicId: string;
    public strengths: Strengths[];

    constructor() {
    }
}

@Injectable()
export class TorrePersonService extends BaseService {
    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'PersonService';
        this.apiUrl += 'https://bio.torre.co/api/bios';
    }

    getByID(publicId: string): Observable<Person> {
        const customUrl = `${this.apiUrl}/${publicId}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response as Person;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    // getList(): Observable<Array<Localidad>> {
    //     const customUrl = `${this.apiUrl}`;
    //     return this.httpClient.get<Array<Localidad>>(customUrl, this.httpOptions)
    //         .pipe(
    //             map(response => {
    //                 return response as Array<Localidad>;
    //             }),
    //             retry(3),
    //             catchError(this.handleError)
    //         );
    // }

    // getListByID(id: string): Observable<Array<Localidad>> {
    //     const customUrl = `${this.apiUrl}/${this.GET_LISTBYID}/${id}`;
    //     return this.httpClient.get<Array<Localidad>>(customUrl, this.httpOptions)
    //         .pipe(
    //             map(response => {
    //                 return response as Array<Localidad>;
    //             }),
    //             retry(3),
    //             catchError(this.handleError)
    //         );
    // }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}