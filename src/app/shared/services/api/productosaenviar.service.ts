import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService, Response } from './base.service';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';
// BORROR

export class ProductosAEnviar {
    public id: string;
    public producto: string;
    constructor() {
        this.id = '';
    }
}

@Injectable()
export class ProductosAEnviarService extends BaseService {
    // readonly GET_LISTBYIDORGANIZACION = 'getlistbyidorganizacion';
    readonly STATUS_SUCCESS =  'success';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'ProductosAEnviar';
        this.apiUrl += 'productosaenviar';
    }

    getList(): Observable<Array<ProductosAEnviar>> {
        const customUrl = `${this.apiUrl}`;
        return this.httpClient.get<Array<ProductosAEnviar>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<ProductosAEnviar>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    getByID(id: number): Observable<ProductosAEnviar> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response[0] as ProductosAEnviar;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    save(entity: ProductosAEnviar): Observable<ProductosAEnviar> {
        if (entity.id && entity.id.length > 0) {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(rol: ProductosAEnviar): Observable<ProductosAEnviar> {
        return this.httpClient.post<ProductosAEnviar>(this.apiUrl, rol, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(entity: ProductosAEnviar): Observable<ProductosAEnviar> {
        const url = `${this.apiUrl}/${entity.id}`;
        return this.httpClient.put<ProductosAEnviar>(url, entity, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: string): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.message as Response);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}
