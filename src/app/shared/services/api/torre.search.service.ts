import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';

import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export class Term {
    term: string;
    
    constructor(term) {
        this.term = term;
    }
}
export class Search {
    name: Term;

    constructor(term) {
        this.name = new Term(term);
    }
}

@Injectable()
export class TorreSearchService extends BaseService {
    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'SearchService';
        this.apiUrl += 'https://search.torre.co/people/_search/';
    }

    search(parameter: string, next: string): Observable<any> {
        const search = new Search(parameter);
        let customUrl = `${this.apiUrl}`;
        customUrl += (next.length == 0) ? '' : `?size=10&after=${next}&aggregate=false`;
        return this.httpClient.post<Search>(customUrl, search, this.httpOptions)
            .pipe(
                map(response => {
                    return response;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}