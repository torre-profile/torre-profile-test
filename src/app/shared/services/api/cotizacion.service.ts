import { Bulto } from './bulto.service';

export class Cotizacion {
    public id: string;
    public tipodoc: number;
    public documento: string;
    public tipoenvio: number;
    public bultos: number;
    public menor: number;
    public valorquiebre: number;
    public nombreyape: string;
    public valormercaderia: number;
    public telefono: string;
    public email: string;
    public facturaa: string;
    public fecha: number;
    public articulos: string;

    public conretiro: number;
    public rem_nombre: string;
    public rem_telefono: string;
    public rem_localidad: string;
    public rem_localidad_desc: string;
    public rem_direccion: string;
    public rem_email: string;

    public conentrega: number;
    public dest_nombre: string;
    public dest_telefono: string;
    public dest_localidad: string;
    public dest_localidad_desc: string;
    public dest_direccion: string;
    public dest_email: string;
    public arr_bultos: Bulto[];

    public preciofinal: string;
    public flete: string;
    public seguro: string;
    public iva: string;

    constructor() {
        this.id = '0';
        this.tipodoc = 1;
        this.arr_bultos = [];
        this.preciofinal = '0';
        this.flete = '0';
        this.seguro = '0';
        this.iva = '0';
    }
}