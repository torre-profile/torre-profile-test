import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';
// BORROR
export class ModuloXRol {
    public id: string;
    public idrol: string;
    public rol: string;
    public idmodulo: string;
    public modulo: string;
    public componente: string;
    public ver: number;
    public editar: number;
    public eliminar: number;

    constructor() {
        this.id = '';
    }
}

@Injectable()
export class ModuloXRolService extends BaseService {
    readonly GET_LISTBYIDROL = 'getlistbyidrol';
    readonly STATUS_SUCCESS = 'success';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'ModuloXRolService';
        this.apiUrl += 'moduloxrol';
    }

    // getList(): Promise<ModuloXRol[]> {
    //   const customUrl = `${this.apiUrl}`;

    //   return this.http
    //     .get(customUrl, { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       const body = response.json();
    //       const entities = body || {} as ModuloXRol[];
    //       return entities;
    //       }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    // getByID(id: number): Promise<ModuloXRol> {
    //   return this.http
    //     .get(this.apiUrl + '/' + id, { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //         const body = response.json();
    //         const entity = body[0] as ModuloXRol;
    //         return entity;
    //       }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    getModulosByIdRol(id: number): Observable<Array<ModuloXRol>> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDROL}/${id}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<ModuloXRol>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    // getListByIDUsuarioIDModulo(idusuario: number, idmodulo: number): Promise<ModuloXRol> {
    // 	const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDUSUARIOIDMODULO}/${idusuario}/${idmodulo}`;
    // 	return this.http
    // 		.get(customUrl, { headers: this.headers })
    // 		.toPromise()
    // 		.then(response => {
    // 			const body = response.json();
    // 			const entity = body[0] as ModuloXRol;
    // 			return entity;
    // 		}
    // 		)
    // 		.catch(e => this.handleError(e));
    // }

    save(entity: ModuloXRol): Observable<ModuloXRol> {
        if (entity.id && entity.id.length > 0) {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(moduloxrol: ModuloXRol): Observable<ModuloXRol> {
        return this.httpClient.post<ModuloXRol>(this.apiUrl, moduloxrol, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(moduloxrol: ModuloXRol): Observable<ModuloXRol> {
        const url = `${this.apiUrl}/${moduloxrol.id}`;
        return this.httpClient.put<ModuloXRol>(url, moduloxrol, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: string): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.message as Response);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }

    // delete(entity: ModuloXRol): Promise<Response> {
    // 	const customUrl = `${this.apiUrl}/${entity.id}`;

    // 	return this.http
    // 		.delete(customUrl, { headers: this.headers })
    // 		.toPromise()
    // 		.then(response => {
    // 			return response.ok;
    // 		})
    // 		.catch(this.handleError);
    // }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}
