import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export const ID_TODOS = '0';

export class Mail {
    public id: string;
    public mail: string;

    constructor() {
    }
}

@Injectable()
export class MailService extends BaseService {

    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.apiUrl += 'mailservice';
    }

    processSend(mail: Mail): Observable<Mail> {
        return this.httpClient.post<Mail>(this.apiUrl, mail, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        // this.modal.alert()
        //     .size('lg')
        //     .isBlocking(true)
        //     .title(this.module)
        //     .keyboard(27)
        //     .body(errorMessage)
        //     .okBtn('Aceptar')
        //     .open();
        return throwError(errorMessage);
    }
}
