import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export const ID_TODOS = '0';
// BORROR
export class Mensaje {
    public id: string;
    public idde: string;
    public de: string;
    public avatar: string;
    public idpara: string;
    public para: string;
    public fecha: number;
    public asunto: string;
    public mensaje: string;

    public icon: any;
    public iconClass: any;
    public cuerpo: string;
    public tag: string;
    public type: string;
    public important: boolean;

    constructor() {
        this.idpara = ID_TODOS;
    }
}

@Injectable()
export class MensajeService extends BaseService {
    readonly FUNC_LISTBYID = 'listbyid';
    readonly FUNC_GETBYID = 'getbyid';

    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'MensajeService';
        this.apiUrl += 'mensajes';
    }

    getListByID(id: string): Observable<Array<Mensaje>> {
        const customUrl = `${this.apiUrl}/${this.FUNC_LISTBYID}/${id}`;
        return this.httpClient.get<Array<Mensaje>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<Mensaje>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    getByID(id: number): Observable<Mensaje> {
        const customUrl = `${this.apiUrl}/${this.FUNC_GETBYID}/${id}`;
        return this.httpClient.get<Mensaje>(customUrl, this.httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError)
            );
    }

    save(entity: Mensaje): Observable<Mensaje> {
        if (entity.id && entity.id !== '') {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(mensaje: Mensaje): Observable<Mensaje> {
        return this.httpClient.post<Mensaje>(this.apiUrl, mensaje, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(mensaje: Mensaje): Observable<Mensaje> {
        return this.httpClient.put<Mensaje>(this.apiUrl, mensaje, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: string): Observable<boolean> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}
