import {v4 as uuidv4} from 'uuid';

export class Bulto {
    public id: string;
    public cotizacionid: string;
    public numero: number;
    public ancho: number;
    public alto: number;
    public largo: number;
    public electrodomestico: number;
    public tamanosplit: number;
    public tamanofreezer: number;
    public tamanoheladera: number;
    public tamanotermotanque: number;
    public tamanotv: number;
    public hpmotor: number;
    public cubierta: number;
    public tipobmc: number;
    public rodado: number;
    public cilindradasmoto: number;
    public cilindradascuatri: number;

    constructor() {
        this.id = uuidv4();
        // this.ancho = ;
        // this.alto = 0;
        // this.largo = 0;
    }
}