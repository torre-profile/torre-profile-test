import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';

import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export class Configuracion {
    public id: string;
    public quiebre: string;

    constructor() {
    }
}

@Injectable()
export class ConfiguracionService extends BaseService {

    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'ConfiguracionService';
        this.apiUrl += 'configuracion';
    }

    getList(): Observable<Array<Configuracion>> {
        const customUrl = `${this.apiUrl}`;
        return this.httpClient.get<Array<Configuracion>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<Configuracion>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}