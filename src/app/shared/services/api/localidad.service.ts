import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { Observable, throwError } from 'rxjs';
import { catchError,  map, retry } from 'rxjs/operators';

import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

export class Localidad {
    public id: string;
    public localidad: string;

    constructor() {
    }
}

@Injectable()
export class LocalidadService extends BaseService {
    readonly GET_LISTBYID = 'listbyid';

    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'LocalidadService';
        this.apiUrl += 'localidad';
    }

    getList(): Observable<Array<Localidad>> {
        const customUrl = `${this.apiUrl}`;
        return this.httpClient.get<Array<Localidad>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<Localidad>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    getListByID(id: string): Observable<Array<Localidad>> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTBYID}/${id}`;
        return this.httpClient.get<Array<Localidad>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<Localidad>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}