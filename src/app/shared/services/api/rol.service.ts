import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService, Response } from './base.service';
import { Modal } from 'ngx-modialog-7/plugins/bootstrap';

// BORROR
export class Rol {
    public id: string;
    public rol: string;
    public idorganizacion: string;
    constructor() {
        this.id = '';
    }
}

@Injectable()
export class RolService extends BaseService {
    readonly GET_LISTBYIDORGANIZACION = 'getlistbyidorganizacion';
    readonly STATUS_SUCCESS =  'success';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'RolService';
        this.apiUrl += 'roles';
    }

    getListByIDOrganizacion(idorganizacion: string): Observable<Array<Rol>> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDORGANIZACION}/${idorganizacion}`;
        return this.httpClient.get<Array<Rol>>(customUrl, this.httpOptions)
            .pipe(
                map(response => {
                    return response as Array<Rol>;
                }),
                retry(3),
                catchError(this.handleError)
            );
    }

    getByID(id: number): Observable<Rol> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response[0] as Rol;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    save(entity: Rol): Observable<Rol> {
        if (entity.id && entity.id.length > 0) {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(rol: Rol): Observable<Rol> {
        return this.httpClient.post<Rol>(this.apiUrl, rol, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(rol: Rol): Observable<Rol> {
        const url = `${this.apiUrl}/${rol.id}`;
        return this.httpClient.put<Rol>(url, rol, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: string): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.message as Response);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }

    protected handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error cliente: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code servidor: ${error.status}\nMessage: ${error.message}`;
        }
        // window.alert(errorMessage);
        this.modal.alert()
            .size('lg')
            .isBlocking(true)
            .title(this.module)
            .keyboard(27)
            .body(errorMessage)
            .okBtn('Aceptar')
            .open();
        return throwError(errorMessage);
    }
}
