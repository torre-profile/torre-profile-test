import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';

// BORROR

export class Profesional {
    public id: string;
    public idsexo: number;
    public idpais: number;
    public pais: string;
    public idprovincia: number;
    public provincia: string;
    public usuario: string;
    public email: string;
    public contrasena: string;
    public apellido: string;
    public nombre: string;
    public dni: string;
    public fecnacimiento: string;
    public notificacion: number;
    public titulo: string;
    public universidad: string;
    public microbiografia: string;
    public credenciales: string;
    public precio: number;
    public fotourl: string;
    public dniurl: string;
    public titulourl: string;
    public matriculaurl: string;
    public fecaprobado: string;
    public fecmodificado: string;
    public fecregistro: string;
    public bloqueoadministrativo: number;
    public especialidades: string;
    public pacientes: number;
    public aprobado: string;
    public incompleto: string;
    public visualizaciones: number;
    public mp_authorization_code: string;
    public sesionesgratis: number;
    public idsocial: string;
    public issocial: number;
    public atendidos: number;

    constructor() {
        this.idsexo = -1;
        this.idpais = -1;
        this.idprovincia = -1;

        // datos para que en login no de error
        this.email = '';
        this.apellido = '';
        this.nombre = '';
        this.dni = '';
        this.fecnacimiento = '';
        this.notificacion = 0;

        // datos para que en login social no de error
        this.contrasena = '';

        // datos en blanco en registro inicial
        this.titulo = '';
        this.universidad = '';
        this.microbiografia = '';
        this.credenciales = '';
        this.precio = 0;
        this.fotourl = '';
        this.dniurl = '';
        this.titulourl = '';
        this.matriculaurl = '';
        this.fecaprobado = '';
        this.fecmodificado = '';
        this.fecregistro = '';
        this.bloqueoadministrativo = 0;
        this.especialidades = '';
        this.visualizaciones = 0;
        this.sesionesgratis = 0;
        this.issocial = 0;
        this.atendidos = 0;
        this.idsocial = '';
    }
}

export class ListaEstudios {
    public id: string;
    public descripcion: string;
    public idprofesional: number;
    public fecultmodif: string;
}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Cache-Control': 'no-cache'
    })
};

@Injectable()
export class ProfesionalService {
    // readonly GET_BYIDSOCIAL = 'byidsocial';
    // readonly GET_HABILITADOS = 'habilitados';
    readonly GET_LISTAESTUDIOS = 'listaestudios';
    // readonly GET_OPINIONESBYID = 'opinionesbyid';
    // readonly GET_REPORTE_PROFESIONALES = 'reporte_profesionales';
    // readonly GET_REPORTE_PREMIUMS = 'reporte_premiums';
    // readonly GET_REPORTE_BASICOS = 'reporte_basicos';
    // readonly GET_RENDIMIENTO_GENERAL = 'rendimiento_general';
    // readonly GET_RENDIMIENTO_PROFESIONAL = 'rendimiento_profesional';
    // readonly GET_REPORTE_HORAS_OFRECIDAS = 'reporte_horas_ofrecidas';
    // readonly GET_RESUMENES = 'resumenes';
    readonly PUT_LOGIN = 'login';
    // readonly PUT_BLOQUEO = 'bloqueo';
    // readonly PUT_APROBACION = 'aprobacion';
    // readonly POST_FOTO = 'foto';
    // readonly POST_DNI = 'dni';
    // readonly POST_TITULO = 'titulo';
    // readonly POST_MATRICULA = 'matricula';
    private apiUrl = '';
    private handleError: HandleError;

    constructor(private httpClient: HttpClient,
        httpErrorHandler: HttpErrorHandler) {
        this.apiUrl = environment.apiUrl + 'profesional';
        this.handleError = httpErrorHandler.createHandleError('ProfesionalService');
    }

    getList(): Observable<Array<Profesional>> {
        const customUrl = `${this.apiUrl}`;

        return this.httpClient.get<Array<Profesional>>(customUrl, httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError('getList', []))
            );
    }

    getByID(id: string): Observable<Profesional> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<Profesional>(customUrl, httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError('getByID', new Profesional()))
            );
    }

    getListaEstudios(): Observable<Array<ListaEstudios[]>> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTAESTUDIOS}`;

        return this.httpClient
            .get<Array<ListaEstudios[]>>(customUrl, httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError('getListaEstudios', []))
            );
    }

    // save(entity: Profesional): Promise<any> {
    //   if (entity.issocial === 1) {
    //     return this.post(entity);
    //   }
    //   if (entity.id) {
    //     return this.put(entity);
    //   }
    //   return this.post(entity);
    // }

    // private post(entity: Profesional): Promise<any> {
    //   return this.http
    //     .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       if (response.status === 200) {
    //         const body = response.json();
    //         return body.message;
    //       } else if (response.status === ProfesionalService.ERROR_NO_AUTORIZADO) {
    //         return ProfesionalService.ERROR_NO_AUTORIZADO;
    //       } else {
    //         return -1;
    //       }
    //     }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    // private put(entity: Profesional): Promise<any> {
    //   const url = `${this.apiUrl}/${entity.id}`;

    //   return this.http
    //     .put(url, JSON.stringify(entity), { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       const body = response.json();
    //       if (body.status === 'success') {
    //         return body.message;
    //       } else if (body.status === ProfesionalService.ERROR_CLAVE_DUPLICADA) {
    //         return ProfesionalService.ERROR_CLAVE_DUPLICADA;
    //       } else if (body.status === 'duplicado') {
    //         return body.message;
    //       } else {
    //         return -1;
    //       }
    //     }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    // login(entity: Profesional): Promise<any> {
    //   const url = `${this.apiUrl}/${this.PUT_LOGIN}`;

    //   return this.http
    //     .put(url, JSON.stringify(entity), { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       if (response.status === 200) {
    //         const body = response.json();
    //         return body.message[0];
    //       } else {
    //         return false;
    //       }
    //     }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    // loginSocial(entity: Profesional): Promise<any> {
    //   const url = `${this.apiUrl}/${this.PUT_LOGINSOCIAL}`;
    //   return this.http
    //     .put(url, JSON.stringify(entity), { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       if (response.status === 200) {
    //         const body = response.json();
    //         return (response.status === 200);
    //       } else {
    //         return false;
    //       }
    //     }
    //     )
    //     .catch(e => this.handleError(e));
    // }

    // delete(entity: Profesional): Promise<Response> {
    //   const customUrl = `${this.apiUrl}/${entity.id}`;

    //   return this.http
    //     .delete(customUrl, { headers: this.headers })
    //     .toPromise()
    //     .then(response => {
    //       if (response.status === 204) {
    //         return response.ok;
    //       } else if (response.status === 202) {
    //         return -1;
    //       }
    //     })
    //     .catch(
    //       error =>
    //         this.handleError(error)
    //     );
    // }

}
