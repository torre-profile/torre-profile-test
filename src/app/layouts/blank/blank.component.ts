import { Component } from '@angular/core';

@Component({
  selector: 'blank-layout',
  templateUrl: './blank.component.html',
  styleUrls: ['./blank.component.scss']
})
export class BlankComponent {
}