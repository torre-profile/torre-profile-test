import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BlankComponent } from './layouts/blank/blank.component';
import { AuthGuard } from './shared/guard/auth.guard';

export const routes: Routes = [
{
    path: '',
    component: BlankComponent,
    children: [
        {
            path: '',
            loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
        }
    ]
},
{
    path: '',
    component: BlankComponent,
    children: [
        {
            path: 'profile',
            loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
        }
    ]
},
{
    path: '**',
    redirectTo: '404'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }), NgbModule],
    exports: [RouterModule]
})
export class AppRoutingModule { }

