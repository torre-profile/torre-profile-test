export const environment = {
  production: true,
  VERSION: '0.1',
  assestUrl: '/cotizador/assets',
  apiUrl: 'https://cors-anywhere.herokuapp.com/',
  DEFAULT_PASS: 'DefaultPass',
};
